import numpy as np

from scipy.integrate import solve_bvp
from scipy.interpolate import interp1d

from seipy.suspension import Suspension, conv_molar_to_SI


class PBSolver:
    """ Solve the Poisson-Boltzmann equation for a two plate system.
    """

    def __init__(self, suspension, potential_bc, ngrid=1000, max_nodes=30000, tol=1e-8):
        self.ngrid = ngrid
        self.suspension = suspension
        self.potential_bc = potential_bc

        self.suspension.to_dimensionless()

        self.energy_interp_func = None
        self.max_nodes = max_nodes
        self.tol = tol

        # convenient reshapes for vectorizing calculations
        self.z = self.suspension.val_ions.reshape((2, 1))
        self.c = self.suspension.conc_ions.reshape((2, 1))
        # self.z = np.asarray(self.suspension.val_ions)
        # self.c = np.asarray(self.suspension.conc_ions)

    def rhs(self, x, y):
        """ Defines the RHS of the PB equation, for use
        with the scipy solve_bvp function. """
        deriv_y0 = y[1, :]
        dx = x[1] - x[0]
        # deriv_y1 =  -1.0/self.suspension.eps \
        #     * np.sum(self.z*self.c*np.exp(-self.z*np.tile(y[0, :],(2, 1))), axis=0)
        deriv_y1 = 0.0
        for k in range(len(self.z)):
            deriv_y1 -= self.z[k] * self.c[k] * np.exp(-self.z[k] * y[0, :])
        deriv_y1 /= self.suspension.eps

        # for k in range(len(self.z)):
        #     deriv_y1 += np.trapz(self.c[k]*np.exp(-self.z[k]*y[0, :]), dx=dx)

        return np.vstack((deriv_y0, deriv_y1))

    def bc(self, ya, yb):
        """ Defines the dirichlet constant potential BC, for
        use with the scipy solve_bvp function. """
        return np.array([ya[0] - self.potential_bc[0], yb[0] - self.potential_bc[1]])

    def solve_linear(self, dist):
        """ Solution of the linearized PB equation using
        the Hogg-Healy-Fuerstenau formula. """
        x = np.linspace(0.0, dist, self.ngrid)
        k = 1.0 / self.suspension.calc_debye_length()
        c1 = self.potential_bc[0]
        c2 = (self.potential_bc[1] - c1 * np.cosh(k * x[-1])) / np.sinh(k * x[-1])
        pot = c1 * np.cosh(k * x) + c2 * np.sinh(k * x)
        force = -k * (c1 * np.sinh(k * x) + c2 * np.cosh(k * x))
        return pot, force, x

    def initial_guess(self, dist):
        """ Intial guess of the potential distribution, for use
        with the scipy solve_bvp function. """
        pot, force, x = self.solve_linear(dist)
        return np.vstack((pot, -force))

    def solve(self, dist):
        """ Solves the actual BVP.

        Note that the solution x-grid is sometimes altered by
        scipy, hence it is also returned here.
        """
        x = np.linspace(0.0, dist, self.ngrid)
        res = solve_bvp(
            self.rhs,
            self.bc,
            x,
            self.initial_guess(dist),
            max_nodes=self.max_nodes,
            tol=self.tol,
        )
        # print('{:.3f}'.format(dist), res.message)
        pot = res.y[0, :]
        force = -res.y[1, :]
        return pot, force, res.x

    def calc_concentration(self, dist, linear=False):
        if not linear:
            pot, force, x = self.solve(dist)
        else:
            pot, force, x = self.solve_linear(dist)
        nions = len(self.z)
        conc = np.zeros((nions, len(x)))
        for k in range(nions):
            conc[k, :] = self.c[k] * np.exp(-self.z[k] * pot)
        return conc, x

    def calc_charge(self, dist, linear=False):
        conc, x = self.calc_concentration(dist, linear=linear)
        return conc * self.z, x

    def calc_pressure(self, dist):
        """ Calculates the disjoining pressure
        as the sum of the osmotic and electrostatic pressures.
        """
        eps = self.suspension.eps
        beta = self.suspension.beta
        z = self.suspension.val_ions
        c = self.suspension.conc_ions
        pot, force, x = self.solve(dist)
        pres1 = -0.5 * eps * force ** 2
        pres2 = 0.0
        for k in range(len(z)):
            pres2 += c[k] * (np.exp(-z[k] * pot) - 1.0)

        return pres1 + pres2, x

    def calc_energy(self, dist):
        """ Calculates the free energy by integrating the
        disjoining pressure.
        """
        if self.energy_interp_func is None:
            self.interpolate_energy()

        # print(np.max(dist))
        return self.energy_interp_func(dist)

    def interpolate_energy(self, npts=1000):
        """ Creates an interpolating function
        for the energy.
        """
        debye = self.suspension.calc_debye_length()
        dstart = 0.0
        dstop = dstart + 5.0
        dist_space = np.linspace(dstart, dstop, npts)
        dstep = dist_space[1] - dist_space[0]
        dmid = dist_space[:-1] + 0.5 * dstep

        # print(dmid[0], dmid[-1])

        # print(debye*30, dmid[0], dmid[-1]*30)

        pres = np.zeros(len(dmid))
        for i, d in enumerate(dmid):
            x = np.linspace(0, d, self.ngrid)
            pres[i] = np.median(self.calc_pressure(d)[0])

        num_include = len(pres) * 5 // 5
        energy = np.flip(np.cumsum(np.flip(pres)) * dstep)[:num_include]
        self.energy_interp_func = interp1d(dmid[:num_include], energy)

    def calc_energy_linear(self, dist):
        """ Calculates the free energy obtained by applying DA to
        the Hogg-Healy-Fuerstenau formula. """
        d = dist
        kappa = 1.0 / self.suspension.calc_debye_length()
        psq = self.potential_bc[0] ** 2 + self.potential_bc[1] ** 2
        cedl1 = 0.5 * self.suspension.eps * kappa * psq
        cedl2 = 2.0 * self.potential_bc[0] * self.potential_bc[1] / psq
        energy = cedl1 * (1.0 - 1.0 / np.tanh(kappa * d) + cedl2 / np.sinh(kappa * d))
        return energy


if __name__ == "__main__":
    import matplotlib.pyplot as plt

    # citrate concentration in M
    conc_salt = conv_molar_to_SI(1.0e-3)
    conc_cit = conc_salt
    # conc_Na = conc_salt
    conc_Na = conc_salt * 3
    val_cit = -3
    val_Na = 1

    # define the AuNP suspension
    susp = Suspension(
        radius=30e-9,
        eps_rel=78.5,
        potential=-2.0 * 25.7e-3,
        conc_ions=[conc_cit, conc_Na],
        val_ions=[val_cit, val_Na],
        hamaker=40e-20,
    )

    # print(susp.calc_debye_length()*1e9)
    fig, ax = plt.subplots()
    markers = ["o", "s", "*"]
    for i, tol in enumerate([1e-8, 1e-9, 1e-10]):
        solver = PBSolver(susp, [-5.0, 2.0], ngrid=1000, max_nodes=30000, tol=tol)
        # solver.interpolate_energy()
        # pot, force, x = solver.solve(d)
        # pot_lin, force_lin, xlin = solver.solve_linear(d)
        d = 3.0
        pres, x = solver.calc_pressure(d)
        plim = np.median(pres)
        # charge, x = solver.calc_charge(d)

        # d = np.linspace(0.1, 1.0, 100)
        # en = solver.calc_energy(d)
        # ax.plot(x, charge[0])
        # ax.plot(x, charge[1])

        # print(solver.calc_charge_total(d, linear=True))

        # chg_tot = np.trapz(chg_Na*conc[1] + chg_cit*conc[0], x=x)

        # ax.plot(xlin, pot_lin)
        ax.plot(x, pres, marker=markers[i])

    ax.set_ylim([plim - 1e-3 * plim, plim + 1.0e-3 * plim])
    plt.show()
