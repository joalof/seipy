import numpy as np
from seipy.suspension import Suspension, conv_molar_to_SI
from seipy.sample import Plate, Sample


def gold_nanoparticles(
    radius=30e-9,
    conc_salt=1.0,  # mM
    val_ions=[-3, 1],
    potential=-1.32 * 25.7e-3,
    hamaker=44.9e-20,
    eps_rel=78.5,
):
    """ Creates an AuNP suspension. """
    conc_salt = conv_molar_to_SI(conc_salt * 1e-3)

    conc1 = conc_salt * abs(val_ions[1])
    conc2 = conc_salt * abs(val_ions[0])
    susp = Suspension(
        radius=30e-9,
        eps_rel=eps_rel,
        potential=potential,
        conc_ions=[conc1, conc2],
        val_ions=val_ions,
        hamaker=hamaker,
    )
    return susp


def substrate(
    potential=-2.23 * 25.7e-3,
    hamaker=7.16e-20,
    cell=np.array([10, 10, 10]),
    mesh_size=(25, 25),
):
    """ Creates a SiO2 substrate. """

    # create the SiO2 substrate
    origin = np.array([0.0, 0.0, 0.0])
    sides = np.array([[cell[0], 0.0, 0.0], [0.0, cell[1], 0.0]])
    subs = Plate(origin, sides, potential=potential, hamaker=hamaker)
    subs.build_mesh(mesh_size)
    return subs


def standard_sample(
    conc_salt=1.0,
    potential_part=-1.5 * 25.7e-3,
    potential_subs=-2.0 * 25.7e-3,
    eps_rel=78.5,
    cell=np.array([10, 10, 10]),
    mesh_size=(25, 25),
):
    susp = gold_nanoparticles(
        conc_salt=conc_salt, potential=potential_part, eps_rel=eps_rel
    )
    subs = substrate(potential=potential_subs, cell=cell, mesh_size=mesh_size)
    sample = Sample(cell, susp, plates=subs)
    return sample


def double_bars(
    gap=3.0,
    sample=standard_sample(),
    potential=2.41 * 25.7e-3,
    hamaker=44.0e-20,
    mesh_size=(25, 25),
    thickness=None,
):
    """ Creates a double bar sample.

    The system consists of two parallel Ni Bars on
    a SiO2 substrate in an AuNP suspension.
    """
    # create the first wall
    length = 350e-9 / sample.suspension.radius
    sample.cell = np.array([gap, length, sample.cell[2]])
    origin = np.array([sample.cell[0], 0.0, 0.0])
    sides = np.array([[0.0, sample.cell[1], 0.0], [0.0, 0.0, 2.33]])
    plate = Plate(origin, sides, potential=potential, hamaker=hamaker)
    plate.build_mesh(mesh_size)
    plate.thickness = thickness
    sample += plate

    # create the second wall
    origin = np.array([0.0, 0.0, 0.0])
    sides = np.array([[0.0, sample.cell[1], 0.0], [0.0, 0.0, 2.33]])
    plate = Plate(origin, sides, potential=potential, hamaker=hamaker)
    plate.build_mesh(mesh_size)
    plate.thickness = thickness
    sample += plate
    return sample
