import numpy as np

CONSTANTS_SI = {
    "eps_0": 8.854187818e-12,
    "k_B": 1.38065e-23,
    "e": 1.60217657e-19,
    "T_room": 298.15,
    "N_A": 6.022140857e23,
}


def estimate_particle_potential(pot_zeta, kappa, dist_zeta):
    """ Estimate the surface potential using GC theory. """
    k_B = CONSTANTS_SI["k_B"]
    T = CONSTANTS_SI["T_room"]
    e = CONSTANTS_SI["e"]
    z = 1.0
    # TODO: minus sign in exp
    pot_surf = (
        4.0
        * k_B
        * T
        / (z * e)
        * np.arctanh(
            np.tanh(z * e * pot_zeta / (4.0 * k_B * T)) * np.exp(kappa * dist_zeta)
        )
    )
    return pot_surf


def conv_molar_to_SI(conc_molar):
    """ 1 molar = 1 mol/L = 1 mol/dm^3 """
    conc_SI = conc_molar * CONSTANTS_SI["N_A"] / 0.1 ** 3
    return conc_SI


class Suspension:
    def __init__(
        self,
        radius=30.0e-9,
        eps_rel=78.5,
        potential=34.0e-3,
        conc_ions=None,
        val_ions=None,
        hamaker=40.0e-20,
    ):

        self.radius = radius
        self.beta = 1.0 / (CONSTANTS_SI["k_B"] * CONSTANTS_SI["T_room"])
        self.eps = CONSTANTS_SI["eps_0"] * eps_rel

        if isinstance(conc_ions, list):
            self.conc_ions = np.asarray(conc_ions)
        else:
            self.conc_ions = conc_ions
        if isinstance(val_ions, list):
            self.val_ions = np.asarray(val_ions)
        else:
            self.val_ions = np.asarray(val_ions)

        # dimensionless energies and potentials
        self.is_dimensionless = False
        self.conversion = {}
        self.conversion["length"] = 1.0 / radius
        self.conversion["energy"] = 1.0 / (CONSTANTS_SI["k_B"] * CONSTANTS_SI["T_room"])
        self.conversion["potential"] = CONSTANTS_SI["e"] * self.conversion["energy"]
        self.conversion["charge"] = 1.0 / CONSTANTS_SI["e"]
        self.conversion["volume"] = 1.0 / radius ** 3

        self.is_dimensionless = False

        self.potential = potential
        self.hamaker_solution = 4.77e-20  # previously 3.7
        self.hamaker = hamaker

    def to_dimensionless(self):
        """ Convert all dimensional properties to
        dimensionless form. """
        if self.is_dimensionless:
            return
        self.eps *= self.conversion["charge"] ** 2 / (
            self.conversion["energy"] * self.conversion["length"]
        )
        self.radius *= self.conversion["length"]
        self.potential *= self.conversion["potential"]
        self.hamaker *= self.conversion["energy"]
        self.hamaker_solution *= self.conversion["energy"]
        self.beta /= self.conversion["energy"]
        self.is_dimensionless = True

        if isinstance(self.conc_ions, list) or isinstance(self.conc_ions, np.ndarray):
            self.conc_ions /= self.conversion["volume"]

    def to_dimensional(self):
        """ Convert all dimensionless properties
        to their dimensional form. """
        if not self.is_dimensionless:
            return
        self.eps /= self.conversion["charge"] ** 2 / (
            self.conversion["energy"] * self.conversion["length"]
        )
        self.radius /= self.conversion["length"]
        self.potential /= self.conversion["potential"]
        self.hamaker /= self.conversion["energy"]
        self.hamaker_solution /= self.conversion["energy"]
        self.beta *= self.conversion["energy"]
        self.is_dimensionless = False

        if self.conc_ions:
            self.conc_ions *= self.conversion["volume"]

    def calc_debye_length(self):
        assert self.val_ions is not None and self.conc_ions is not None
        if self.is_dimensionless:
            e = 1.0
        else:
            e = 1.0 / self.conversion["charge"]
        z = self.val_ions
        c = self.conc_ions
        debye = np.sqrt(self.eps / (self.beta * e ** 2 * np.sum(c * z ** 2)))
        return debye
