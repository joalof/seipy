from seipy.suspension import Suspension, CONSTANTS_SI
from seipy.pbsolver import PBSolver
import numpy as np


class Plate:
    """ A rectangular plate formed by two vectors representing the sides
    relative a specified point of local origin (i.e. a corner of the plate).
    """
    def __init__(self, origin, sides, potential=None, hamaker=40e-20):
        self.origin = np.asarray(origin)
        self.sides = np.asarray(sides)
        self.potential = potential
        self.hamaker = hamaker
        self.mesh = None
        self.mesh_size = None
        self.area_elem = None
        self.thickness = None

        # dimensionless energies and potentials
        self.is_dimensionless = False
        self.conversion = {}
        self.conversion["energy"] = 1.0 / (CONSTANTS_SI["k_B"] * CONSTANTS_SI["T_room"])
        self.conversion["potential"] = CONSTANTS_SI["e"] * self.conversion["energy"]
        self.conversion["charge"] = 1.0 / CONSTANTS_SI["e"]

    def __repr__(self):
        r = ["Plate instance with:\n"]
        r += ["origin={},\n".format(self.origin)]
        r += ["sides={},\n".format(self.sides)]
        r += ["hamaker={:.4f},\n".format(self.hamaker)]
        r += ["potential={:.4f}\n".format(self.potential)]
        return "".join(r)

    def to_dimensionless(self):
        """ Convert all dimensional properties to
        dimensionless form. """
        if self.is_dimensionless:
            return
        self.potential *= self.conversion["potential"]
        self.hamaker *= self.conversion["energy"]
        self.is_dimensionless = True

    def to_dimensional(self):
        """ Convert all dimensionless properties
        to their dimensional form. """
        if not self.is_dimensionless:
            return
        self.is_dimensionless = False
        self.potential /= self.conversion["potential"]
        self.hamaker /= self.conversion["energy"]
        self.is_dimensionless = False

    @property
    def normal(self):
        """ Surface normal for the plate.
        """
        n = np.cross(self.sides[0], self.sides[1])
        return n / np.linalg.norm(n)

    def build_mesh(self, size=(20, 20), use_plate_origin=False):
        """ Returns a centered mesh of the plate with coordinates
        given relative to the global origin, rather than the
        local plate origin (unless requested). """
        self.mesh_size = size
        span0 = np.linspace(0, 1.0, size[0] + 1)
        span1 = np.linspace(0, 1.0, size[1] + 1)
        ds0 = span0[1] - span0[0]
        ds1 = span1[1] - span1[0]
        area_elem = (
            ds0
            * np.linalg.norm(self.sides[0, :])
            * ds1
            * np.linalg.norm(self.sides[1, :])
        )
        span0 = span1[:-1] + 0.5 * ds0
        span1 = span1[:-1] + 0.5 * ds1
        mesh_coeffs = np.transpose(
            [np.tile(span0, len(span1)), np.repeat(span1, len(span0))]
        )
        if use_plate_origin:
            self.mesh = np.dot(mesh_coeffs, self.sides)
        else:
            self.mesh = np.dot(mesh_coeffs, self.sides) + self.origin
        self.area_elem = area_elem

    def view_mesh(self, cell=None):
        """ Produces a 3D visualization of the mesh. """
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D
        from seipy.tools import set_axes_equal3D

        if self.mesh is None:
            self.build_mesh()

        fig = plt.figure()
        ax = fig.add_subplot(111, projection="3d")
        ax.scatter(self.mesh[:, 0], self.mesh[:, 1], zs=self.mesh[:, 2])
        if cell is not None:
            ax.set_xlim([0.0, cell[0]])
            ax.set_ylim([0.0, cell[1]])
            ax.set_zlim([0.0, cell[2]])
        plt.show()

    def rescale(self, scale):
        self.origin *= scale
        self.sides[0] *= scale
        self.sides[1] *= scale
        if self.mesh is not None:
            self.build_mesh(self.mesh_size)


class Sample:
    def __init__(self, cell, suspension, plates=None):

        self._cell = cell
        self.suspension = suspension
        self.plates = []
        self.is_dimensionless = False
        self.solvers = None
        if plates:
            if not isinstance(plates, list):
                plates = [plates]
            for plate in plates:
                self.plates.append(plate)

    @property
    def cell(self):
        return self._cell

    @cell.setter
    def cell(self, new_cell):
        # rescale the plates according to the new cell
        scale = new_cell / self._cell
        for plate in self.plates:
            plate.rescale(scale)
        self._cell = new_cell

    def calc_effective_hamaker(self, plate):
        ham_part = self.suspension.hamaker
        ham_sol = self.suspension.hamaker_solution
        ham_plate = plate.hamaker
        ham_eff = (np.sqrt(ham_part) - np.sqrt(ham_sol)) * (
            np.sqrt(ham_plate) - np.sqrt(ham_sol)
        )
        return ham_eff

    def extend(self, plate):
        self.plates.append(plate)

    def __add__(self, plate):
        self.extend(plate)

    def __iadd__(self, plate):
        self.extend(plate)
        return self

    def to_dimensionless(self):
        if not self.is_dimensionless:
            self.suspension.to_dimensionless()
            for plate in self.plates:
                plate.to_dimensionless()
            self.is_dimensionless = True

    def to_dimensional(self):
        if self.is_dimensionless:
            self.suspension.to_dimensional()
            for plate in self.plates:
                plate.to_dimensional()
            self.is_dimensionless = False

    def add_plate(
        self, origin, sides, potential=None, hamaker=None, mesh_size=(50, 50)
    ):
        plate = Plate(origin, sides, potential=potential, hamaker=hamaker)
        plate.build_mesh(mesh_size)
        self += plate

    def init_solvers(self, npts_interp=1000):
        """ Initializes solvers for the non-linear PB equation.

        One solver per unique plate potential is required. The solvers
        are stored in a dict where the (truncated) plate potentials act as keys.
        """
        self.to_dimensionless()
        self.solvers = {}
        for plate in self.plates:
            self.solvers[round(plate.potential, 4)] = PBSolver(
                self.suspension, [plate.potential, self.suspension.potential],
            )

        for key, solver in self.solvers.items():
            solver.interpolate_energy(npts_interp)

    def calc_energy(self, particle_com, linearized=True, separate=False):
        """ Calculate the DLVO energy using SEI. """
        self.suspension.to_dimensionless()

        if not linearized and self.solvers is None:
            self.init_solvers()

        # shorthands
        x = np.asarray(particle_com)
        a = self.suspension.radius
        k = 1.0 / self.suspension.calc_debye_length()

        # loop over all plates present in the sample
        # and add up dlvo energies
        en_vdw = 0.0
        en_edl = 0.0

        for plate in self.plates:
            plate.to_dimensionless()
            if plate.mesh is None:
                plate.build_mesh((50, 50))
            num_mesh = plate.mesh.shape[0]

            ham_eff = self.calc_effective_hamaker(plate)
            psq = self.suspension.potential ** 2 + plate.potential ** 2
            cedl1 = 0.5 * self.suspension.eps * k * psq
            cedl2 = 2.0 * plate.potential * self.suspension.potential / psq
            darea = plate.area_elem

            # There will be an interaction for all mesh elements  whose distance
            # to the particle (COM) projected onto the plate plane is less than a.
            displ = plate.mesh - x
            displ_normal = np.dot(
                np.dot(displ, plate.normal).reshape((num_mesh, 1)),
                plate.normal.reshape((1, 3)),
            )
            displ_tangent = displ - displ_normal
            dist = np.linalg.norm(displ_tangent, axis=1)
            displ_normal = displ_normal[dist < a, :]
            dist = dist[dist < a]
            h = np.linalg.norm(displ_normal, axis=1) - a
            z = np.sqrt(a ** 2 - np.multiply(dist, dist))
            hbot = h + a - z
            htop = h + a + z

            # vdW contributions
            en_vdw += -ham_eff * darea * (np.sum(1.0 / (12.0 * np.pi * hbot ** 2)))
            en_vdw -= -ham_eff * darea * (np.sum(1.0 / (12.0 * np.pi * htop ** 2)))

            # correct for finite thickness
            if plate.thickness:
                t = plate.thickness
                en_vdw += (
                    ham_eff * darea * (np.sum(1.0 / (12.0 * np.pi * (hbot + t) ** 2)))
                )
                en_vdw -= (
                    ham_eff * darea * (np.sum(1.0 / (12.0 * np.pi * (htop + t) ** 2)))
                )

            # EDL contributions from linear or full PB
            if linearized:
                en_edl += (
                    cedl1
                    * darea
                    * (
                        np.sum(
                            1.0
                            - 1.0 / np.tanh(k * a * hbot)
                            + cedl2 / np.sinh(k * a * hbot)
                        )
                    )
                )
                en_edl -= (
                    cedl1
                    * darea
                    * (
                        np.sum(
                            1.0
                            - 1.0 / np.tanh(k * a * htop)
                            + cedl2 / np.sinh(k * a * htop)
                        )
                    )
                )
            else:
                key = round(plate.potential, 4)
                en_edl += darea * np.sum(self.solvers[key].calc_energy(hbot))
                en_edl -= darea * np.sum(self.solvers[key].calc_energy(htop))

        if separate:
            return en_edl, en_vdw
        else:
            return en_edl + en_vdw

    def view(self):
        """ Produces a 3D visualization of the mesh. """
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D
        from seipy.tools import set_axes_equal3D

        fig = plt.figure()
        ax = fig.add_subplot(111, projection="3d")
        ax.set_aspect("equal")

        for plate in self.plates:
            if plate.mesh is None:
                plate.build_mesh()
            ax.scatter(plate.mesh[:, 0], plate.mesh[:, 1], zs=plate.mesh[:, 2])

        set_axes_equal3D(ax)
        # ax.set_xlim([0.0, self.cell[0]])
        # ax.set_ylim([0.0, self.cell[1]])
        # ax.set_zlim([0.0, self.cell[2]])
        plt.show()


if __name__ == "__main__":
    pass
