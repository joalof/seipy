from setuptools import setup

setup(name='seipy',
      version='0.1',
      description='DLVO with Surface Element Integration',
      url=None,
      author='Joakim Loefgren',
      author_email='joalof@chalmers.se',
      license='MIT',
      platforms=['linux'],
      packages=['seipy'],
      # scripts=['bin/my_script.py'],
      long_description='''Calculate colloidal interactions using DLVO theory with Surface Element Integration.''',
      # install_requires=[
      #     'numpy',
      #     'ase',
      #     'matplotlib',
      #     ],
      )
