## Seipy
This project implements the surface element integration (SEI) method (1) for calculating interaction free energies and forces between convex objects within DLVO theory. The current iteration of the code supports calculating the interaction free energy between between a spherical particle and one or more flat surfaces and can thus easily be used to simulate deposition experiments. For calculation of the electrostatic contributions, full solution of the non-linear Poisson-Boltzmann equation is supported along with the linearized version. In the former case, an efficient interpolation scheme is used to reduce the computational burden.

### Credits
If you use ``seipy`` in your research, please cite:

   * *Understanding Interactions Driving the Template-Directed Self-Assembly of Colloidal Nanoparticles at Surfaces*
       Johnas Eklöf-Österberg, Joakim Löfgren, Paul Erhart, and Kasper Moth-Poulsen
       The Journal of Physical Chemistry C **2020** 124 (8), 4660-4667
       DOI: 10.1021/acs.jpcc.0c00710

### References
(1) Bhattacharjee S., Elimelech, M., *Surface Element Integration: A Novel Technique for Evaluation of DLVO Interaction between a Particle and a Flat Plate*, Journal of Colloid and Interface Science 193(2) 1997, pp. 273-285
